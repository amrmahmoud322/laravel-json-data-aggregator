<?php

namespace App\Http\Controllers;

use App\Services\DataProviderService;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

class UsersController extends Controller
{
    private $dataProviderService;

    public function __construct(DataProviderService $dataProviderService)
    {
        $this->dataProviderService = $dataProviderService;
    }

    /**
     * Retrieve data from DataProviderService and return as JSON response.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request) : JsonResponse {
        $data = $this->dataProviderService->getDataFromFiles();

        // Apply filters if provided in the request
        if ($request->has('provider')) {
            $data = $this->filterByProvider($data, $request->input('provider'));
        }
        
        if ($request->has('statusCode')) {
            $data = $this->filterByStatusCode($data, $request->input('statusCode'));
        }

        if ($request->has('balanceMin')) {
            $data = $this->filterByMinBalance($data, $request->input('balanceMin'));
        }

        if ($request->has('balanceMax')) {
            $data = $this->filterByMaxBalance($data, $request->input('balanceMax'));
        }

        if ($request->has('currency')) {
            $data = $this->filterByCurrency($data, $request->input('currency'));
        }

        return response()->json(['data' => $data]);
    }

    private function filterByProvider($data, $provider)
    {
        return collect($data)->where('provider', $provider)->all();
    }

    private function filterByStatusCode($data, $statusCode)
    {
        return collect($data)->where('status', $statusCode)->all();
    }

    private function filterByMinBalance($data, $minBalance)
    {
        return collect($data)->where('amount', '>=', $minBalance)->all();
    }

    private function filterByMaxBalance($data, $maxBalance)
    {
        return collect($data)->where('amount', '<=', $maxBalance)->all();
    }

    private function filterByCurrency($data, $currency)
    {
        return collect($data)->where('currency', $currency)->all();
    }
}
