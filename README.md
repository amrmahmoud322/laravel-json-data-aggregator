# Laravel JSON Data Aggregator

## Introduction

The Laravel JSON Data Aggregator is a powerful API-driven application designed to process and aggregate JSON data from an unlimited number of files stored in the ```./public/data_provider``` directory. It provides a seamless solution for standardizing and filtering data to meet specific requirements.

## Features

- Unlimited JSON Files: The system can dynamically ingest and process any number of JSON files placed in the designated ```./public/data_provider``` folder.
- API Endpoint: Access the aggregated data through the API endpoint at ```http://127.0.0.1:8000/api/v1/users```.
- Data Standardization: The application normalizes data from various JSON files, ensuring a consistent format for easy consumption.
- Filtering Capabilities:
--Provider: Filter results by the data provider.
--Status Code: Filter by status code (authorized, declined, refunded).
--Minimum Balance: Filter by minimum balance value.
--Maximum Balance: Filter by maximum balance value.
--Currency: Filter by currency type.

## How it Works
### Dynamic Data Ingestion:
-Simply place any number of JSON files in the ./public/data_provider directory. The application will automatically process and aggregate their contents.
### Standardization Process:
-The system intelligently maps each file's data to a common standard, ensuring uniformity across all entries.
### API Access:
-Access the aggregated data via a user-friendly API endpoint at ```http://127.0.0.1:8000/api/v1/users```.
### Filtering Options:
-Utilize various parameters to narrow down the dataset based on provider, status code, balance range, and currency type.

## JSON File Structure
Each JSON file must adhere to the following structure:
```
{
    "data": [
        {
            "balance": 200,
            "currency": "USD",
            "email": "parent1@parent.eu",
            "status": 200,
            "created_at": "2018-11-30",
            "id": "d3d29d70-1d25-11e3-8591-034165a3a613"
        },
        {
            "balance": 100,
            "currency": "USD",
            "email": "parent2@parent.eu",
            "status": 100,
            "created_at": "2020-11-30",
            "id": "d3d29f50-2P15-25s3-6963-036965a3m698"
        }
    ],
    "mapKeys": {
        "amount": "balance",
        "currency": "currency",
        "email": "email",
        "status_code": "status",
        "date": "created_at",
        "id": "id"
    },
    "mapStatus": {
        "100" : "authorised",
        "200" : "decline",
        "300" : "refunded"
    },
    "provider": "DataProvider A"
}
```

## Explanation
- "data": Contains an array of objects representing individual data entries from the provider
- "mapKeys": Defines how the keys from the original data map to the standardized keys. For example, "amount" maps to "balance", etc.
- "mapStatus": Specifies how status codes are mapped to their corresponding status labels. For instance, "100" maps to "authorised", etc.
- "provider": Indicates the name of the data provider.


Make sure to follow this structure for each JSON file placed in the ```./public/data_provider``` directory for seamless aggregation and processing.


## Installation

To get started with this project, follow these steps:

1. Clone the repository.

2. Navigate to the project directory.

3. Install PHP dependencies using Composer:
   ```shell
   composer install
   ```

4. Create a `.env` file by copying `.env.example`:
   ```shell
   cp .env.example .env
   ```

5. Generate a new Laravel application key:

   ```shell
   php artisan key:generate
   ```

6. Start the Laravel development server:

   ```shell
   php artisan serve
   ```

   Access the application at `http://127.0.0.1:8000`.

## Usage

1. With the Laravel development server running, make API requests to the `/api/v1/users` endpoint to filter and retrieve JSON data. You can use query parameters to specify filters, such as `provider`, `statusCode`, `balanceMin`, `balanceMax`, and `currency`.

   Example request:

   ```
   GET http://127.0.0.1:8000/api/v1/users?provider=DataProviderX&statusCode=authorised
   ```

   This request will retrieve data from `DataProviderX` with a `statusCode` of `authorised`.

## Testing

To run unit tests, use the following command:

```shell
php artisan test
```

## Docker Support

This project includes Docker support for containerization. You can use the provided `Dockerfile` and `docker-compose.yml` files to set up and run the project in a Docker container.

### Building and Running with Docker

1. Clone the repository as mentioned above.

2. Navigate to the project directory.

3. Build and start the Docker containers:

   ```shell
   docker-compose up -d
   ```

4. Access the application at `http://localhost:8000` in your web browser.

5. When done, stop the containers:
   ```shell
   docker-compose down
   ```